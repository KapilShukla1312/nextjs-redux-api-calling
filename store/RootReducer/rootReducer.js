import CartReducer from "../redux/reducers/Cart";
import { combineReducers } from "redux";
import apiReducer from "../redux/reducers/apiDetail";

const rootReducer = combineReducers({
  cart: CartReducer,
  apiD: apiReducer,
});

export default rootReducer;
