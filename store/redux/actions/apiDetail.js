import { NEW_API } from "../../types";
import axios from "axios";

export function apiAction() {
  return async (disptach) => {
    const getJson = await axios("https://jsonplaceholder.typicode.com/photos");

    const resData = getJson.data;

    disptach({
      type: NEW_API,
      data: resData,
    });
  };
}
