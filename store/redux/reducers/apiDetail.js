import { NEW_API } from "../../types";

const initialState = {
  api: [],
};

function apiReducer(state = initialState, action) {
  switch (action.type) {
    case NEW_API:
      //console.log("resData -->>", action.data);
      return {
        ...state,
        api: action.data,
      };

    default:
      return state;
  }
}

export default apiReducer;
