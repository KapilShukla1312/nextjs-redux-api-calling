import { useEffect } from "react";
import CartList from "../src/components/CartDesigns/CartList";
import { apiAction } from "../store/redux/actions/apiDetail";
import { useDispatch, useSelector } from "react-redux";
//import axios from "axios";

export default function Home(props) {
  //console.log(props.newData);

  const dispatch = useDispatch();

  const getData = useSelector((state) => state.apiD.api);

  console.log("getData -->>", getData);

  useEffect(() => {
    dispatch(apiAction());
  }, []);

  return (
    <div>
      <CartList data={getData} />
    </div>
  );
}

////---->>>> calling api using axios inside the index.js file in getServerSideProps() method ---

//export async function getServerSideProps() {
//  const getJson = await axios("https://jsonplaceholder.typicode.com/photos");

//  const resData = getJson.data;

//  return {
//    props: {
//      newData: resData,
//    },
//  };
//}